<h1 style="color: #EB894C; margin:12px"> 
MonokaiToolkit Extension ( Beta version )
</h1>

***
>***MonokaiToolkit*** is a free application that provides you with a powerful set of tools to help you experience and manage Facebook in the best way.
>The application **does not store** any information about the user. Your account may be occurred [`checkpoint`](https://support.liveagent.com/851456-Facebook-integration-error---the-user-is-enrolled-in-a-blocking-logged-in-checkpoint#:~:text=This%20error%20message%20means%20that,to%20be%20a%20real%20name.) if you log in with your Facebook account. To avoid it, please login with QR. We are not responsible for the checkpoint of your account being scanned by Facebook
***



Quick links:
* [Installation](#installation)
* [How to use](#how-to-use)
* [Facebook Tools](#1-facebook-tools)
* [Facebook Automated Tools](#2-facebook-automated-tools)
* [Facebook Schedule Tools](#3-facebook-schedule-tools)
* [Develop & Test](#dev&test)
***
### Develop & Test
Recommend to use a *sub facebook account* or *enable two-factor authentication* during development and testing.
***
### Features
[Facebook Tools:](#1-facebook-tools)
* [Interaction cleaner name](#interaction-cleaner-name)
* [Unsent tool](#unsent-tool)
* [Download inbox](#download-inbox)
* [Spread messages](#spread-messages)
* [Upload long story](#upload-long-story) 
* [Scan story](#scan-story)
* [Avatar guard](#avatar-guard) *(Work on mobile)*
* [Profile Analysis](#profile-analysis)
* [Set post privacy](#set-post-privacy)
* [Push posts](#push-posts)
* [Cleanup Personal Page](#cleanup-personal-page)
* [Get ID from URL](#get-id-from-url)
* [Clean up your friend list](#clean-up-your-friend-list)  
* [Clean friend requests](#clean-friend-requests)
* [Custom story reactions](#custom-story-reactions)
* [Interaction tools](#interaction-tools)
* [Poke friends](#poke-friends)
* [Download facebook video](#download-facebook-video)
* [Export data](#export-data)
* [Top messages](#top-messages)
* [Top Interaction](#top-interaction)
* Facebook statistics (Not release yet)
* Scan data (Not release yet)

[Facebook Automated Tools:](#2-facebook-automated-tools)

* [Auto reply messages](#auto-reply-messages) *(Integrate OpenAI GPT)* 
* [Auto reactions](#auto-reactions)
* [Auto comments](#auto-comments)
* [Auto process friend requests](#auto-process-friend-requests)
* [Auto process group posts](#auto-process-group-posts)

[Facebook Schedule Tools:](#3-facebook-schedule-tools)

* [Schedule send message](#schedule-send-message)
* [Schedule post timeline](#schedule-post-timeline)
***
### Installation

If you already have built extension file. Let's go to *Manage extensions setting step*

Clone this repository:
```sh
$ git clone https://gitlab.com/monokaijs/mcrx
```
or
```sh
$ git clone git@gitlab.com:monokaijs/mcrx.git
```
Install node_modules:
```sh
$ npm install
```
Build dist file:
```sh
$ npm run build
```

Manage extensions setting:

1.Setting extensions mode -> Enable *Developer mode*

2.Load built extension file 

![img_17.png](assets/images/img_17.png)

***
### How to use


![img_15.png](assets/images/img_15.png)


### 1. Facebook Tools
***
>##### Interaction cleaner name
**Note: This action cannot be undone**

Feature: Delete your interactions for a period of time

Usage: Select **mode** and **range date** that you want to clean then click on button **Start Clean**

![img_1.png](assets/images/interaction-tool.png)
***
>##### Unsent tool
**Note: This action cannot be undone**

Feature: Unsent all messages in one inbox

Usage: Enter your Facebook profile ID or URL or select friend in Friend Picker who you want to unsent messages.

![img.png](assets/images/img.png)

![img_1.png](assets/images/img_1.png)

Select unsent filter then click on **Start unsent messages** button to start unsent process

![img_2.png](assets/images/img_2.png)

***
>##### Download inbox

Feature: Download all messages in inbox

Usage: Enter your Facebook profile ID or URL or select friend in Friend Picker who you want to download content of conversation. Then click on button **Start Download** to download inbox file.

![img_3.png](assets/images/img_3.png)

***
>#### Spread messages

Feature: Spread message to selected users 

Usage: Enter your Facebook profile ID or URL or select friend in Friend Picker 

![img_4.png](assets/images/img_4.png)

Then enter message content and click on **Send message** button to spread messages  to selected friends.

![img_5.png](assets/images/img_5.png)

***
>#### Upload long story

**Note**: Recommend slow mode to avoid errors

Feature: Upload video with unlimited length

Usage:
>1. Upload by video:

Select file to upload to your story

![img_6.png](assets/images/img_6.png)

>2. Upload unlimited reel

Enter reel post text and select file to upload reel video

![img_7.png](assets/images/img_7.png)

>3. Upload by ID

Enter video url and click button **Post a long story**

![img_8.png](assets/images/img_8.png)

***

>#### Scan story

Feature: Scan data related to story

![img_9.png](assets/images/img_9.png)Usage: 

>#### Avatar guard

**Note:** Only work on mobile app ( Install Monokai Toolkit )

Feature: Turn on the avatar protection shield to prevent spoofing

![img_10.png](assets/images/img_10.png)

***

>#### Profile Analysis

Feature: Analyze personal page interaction data

Usage: Enter your Facebook profile ID or URL or select friend in Friend Picker and set limit of posts then click on button **Start Scan** 

![img_11.png](assets/images/img_11.png)

>#### Set post privacy

Feature: Set privacy for a series of posts on your profile

Usage: Choose range time that you want to scan and set privacy then click on button "Start Scan"

![img_12.png](assets/images/img_12.png)

***

>#### Push posts

Feature: Anti-drift for posts

![img_13.png](assets/images/img_13.png)

>### Cleanup personal page

Feature: Clean up profile posts and information

Usage: Select range date and start scan to load list of posts


![img_14.png](assets/images/img_14.png)

>#### Get ID from URL

Feature: Get ID from URL on Facebook

Usage: 

Select your friend who you want to get friend's Facebook ID

![img_18.png](assets/images/img_18.png)

Enter URL and click on "search" button to get *User ID*

![img_21.png](assets/images/img_21.png)

Enter URL and click on "search" button to get *Post ID*

![img_20.png](assets/images/img_20.png)

>#### Clean up your friend list

Feature: Remove people from your friends list as needed

Usage:

I. Filter non-interactive friends 

1.Scan information

![img.png](assets/images/automated-test/img.png)

2.Filter data and remove people from friend list

2.1 Simple mode


![img_1.png](assets/images/automated-test/img_1.png)

2.2 Advance mode

![img_2.png](assets/images/automated-test/img_2.png)

II. Filter locked friends

![img_3.png](assets/images/automated-test/img_3.png)

>#### Clean friend requests

Feature: Manage friend request easily

![img_4.png](assets/images/automated-test/img_4.png)

![img_5.png](assets/images/automated-test/img_5.png)

>#### Custom story reactions

Feature: React story with custom emoticons

![img_6.png](assets/images/automated-test/img_6.png)

>#### Interaction tools

Feature: 

1.React selected friend posts

![img_7.png](assets/images/automated-test/img_7.png)

2. Filter comments in posts

![img_8.png](assets/images/automated-test/img_8.png)

>#### Poke friends

Feature: Automatically poke friends in list friends

![img_9.png](assets/images/automated-test/img_9.png)

>#### Download facebook video

Feature: Download facebook on video by video link

![img_10.png](assets/images/automated-test/img_10.png)

>#### Export data

Feature: 

I. Backup friends

Usage: Select file extension that you want to export 

![img_11.png](assets/images/automated-test/img_11.png)

II. Add friends 

Usage: Import list friends file. You can add friends in table data.  

![img_13.png](assets/images/automated-test/img_13.png)

>#### Top messages

Feature: List of people who have texted you the most

![img_14.png](assets/images/automated-test/img_14.png)

>#### Top Interaction

Feature: See who interact with you most on Facebook

![img_15.png](assets/images/automated-test/img_15.png)

### 2. Facebook Automated Tools
***

>#### Auto reply to messages 

Feature: Auto reply when receiving messages on facebook

Usage:

Enable or disable mode (Private message or Group messages)

![img_16.png](assets/images/automated-test/img_16.png)

I.Add rule:

![img_17.png](assets/images/automated-test/img_17.png)

1.Enter basic info and priority level

![img_18.png](assets/images/automated-test/img_18.png)

2.Add filter and choose filter mode

![img_19.png](assets/images/automated-test/img_19.png)

![img_20.png](assets/images/automated-test/img_20.png)

3. Add reply actions

![img_22.png](assets/images/img_22.png)

4. Click on button "Add rule"

![img_23.png](assets/images/img_23.png)

II. Export & import rules

![img_24.png](assets/images/img_24.png)

![img_25.png](assets/images/img_25.png)

III. History *(Not release yet)*

>#### Auto reactions

Feature: The auto reactions feature helps you automactically react with newsfeed posts

![img_26.png](assets/images/img_26.png)

3. First, to get an overview of all available command line options, you can either run
[`fd -h`](#command-line-options) for a concise help message or `fd --help` for a more detailed
version.


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ngocdunghk15/mcrx-document.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ngocdunghk15/mcrx-document/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

